﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ViazyNetCore.Authorization.Modules.Repository
{
    public interface IOperationLogRepository : IBaseRepository<OperationLog, long>
    {

    }
}
